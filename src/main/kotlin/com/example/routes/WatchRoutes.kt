package com.example.routes

import com.example.models.Watch
import com.example.models.watchStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.watchRouting() {
    route("watches") {
        get {
            if (watchStorage.isNotEmpty()) call.respond(watchStorage)
            else call.respondText("No hi ha contingut", status = HttpStatusCode.NoContent)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (watch in watchStorage){
                if (watch.id == id) return@get call.respond(watch)
            }
            call.respondText("No s'ha trobat el rellotge amb $id", status = HttpStatusCode.NotFound)
        }
        post {
            val watch = call.receive<Watch>()
            watchStorage.add(watch)
        }

    }
}