package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Watch(val id: String, val price: Double, val brand: String, val genre: String)

val watchStorage = mutableListOf<Watch>()